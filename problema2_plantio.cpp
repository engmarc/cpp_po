//
//  problema2_plantio.cpp
//  projeto prv-agv
//
//  Created by marcos♞ on 30/06/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <stdio.h>
#include <ilcplex/ilocplex.h>

using namespace std;


int main() {
    //criando o ambiente dnv
    IloEnv env;
    
    try {
        //entradas
        int qtde_fazendas=3;
        int preco_culturas[3]={5000,4000,1800};
        int max_area_fazendas[3]={400,650,350};
        int max_area_culturas[3]={660,880,400};
        
        //MODELO
        IloModel problema_plantio(env,"problema plantio");
        IloCplex cplex_plantio(problema_plantio);
        
        //expressao de decisao
        IloNumVarArray recebe_decisao_x(env,qtde_fazendas,0,IloInfinity);
        
        
        //funcao objetivo
        IloExpr funcao_objetivo_maximizar(env);
        for(int i=0;i<qtde_fazendas;i++){
            funcao_objetivo_maximizar += preco_culturas[i]*recebe_decisao_x[i];
            cout<< funcao_objetivo_maximizar<< endl;
        }
        problema_plantio.add(IloMaximize(env, funcao_objetivo_maximizar));
        
        
        //restricao area de cada fazenda com a proporcao
        IloExpr restricao_area_fazenda(env);
        for(int i=0; i<qtde_fazendas;i++){
            restricao_area_fazenda+=max_area_fazendas[i]*recebe_decisao_x[i];
            
            cout<< restricao_area_fazenda<< endl;
        }
        problema_plantio.add(restricao_area_fazenda<=max_area_fazendas[1]);
        //restricao agua para cada cultura
        IloExpr restricao_agua_cultura(env);
        for(int i=0;i<qtde_fazendas;i++){
            restricao_agua_cultura+= max_area_culturas[i]*recebe_decisao_x[i];
        }
        problema_plantio.add(restricao_agua_cultura<=max_area_culturas[2]);
        
        if(cplex_plantio.solve()){
            
            cout<< "encontramos a solucao maxima : " << cplex_plantio.getObjValue() << endl;
        }
        
        IloNumArray solucao(env,qtde_fazendas);
        cplex_plantio.getValues(solucao,recebe_decisao_x);
        
        for(int i= 0;i<qtde_fazendas;i++){
            
            cout<< "lucro de cada fazenda sei la : "<< solucao[i]<< endl;
        }
    } catch (IloException e ) {
        
        cerr << "Exception caught: " << e << endl;

    }catch(...){
        
        cerr << "Unknown exception caught!" << endl;
    }
    env.end();
    return 0;
}
