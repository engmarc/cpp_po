//
//  problema3_tintas.cpp
//  mestrado_cplex
//
//  Created by marcos♞ on 13/07/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <stdio.h>
#include <ilcplex/ilocplex.h>

using namespace std;


int main(){
    
    IloEnv env;
    
    
    try {
        int num_depot=3;
        int custo_fabs[3][3]={{8,5,6},
                            {15,10,12},
                            {3,9,10}};
        int fab_max[]={120,80,80};
        int depot_max[]={150,70,60};
        
        IloModel problematransporte(env,"problema do transporte");
        IloCplex cplex_transporte(problematransporte);
        
        
        IloArray<IloNumVarArray> var_dec(env,num_depot);
        for(int i=0;i<num_depot;i++)
            var_dec[i]= IloNumVarArray(env,num_depot,0,IloInfinity);
        
        //funcao obj
        
        IloExpr fun_ojb(env);
        for(int i=0;i<num_depot;i++){
            for(int j=0;j<num_depot;j++)
                fun_ojb+= custo_fabs[i][j]*var_dec[i][j];
        }
        
        //cout<< fun_ojb<< endl;
        problematransporte.add(IloMinimize(env,fun_ojb));
        
        //restricoes
        
        IloExpr rest_depot(env);
         for(int i=0;i<num_depot;i++){
             for(int j=0;j<num_depot;j++){
                       rest_depot+= var_dec[i][j];
             
             }
             problematransporte.add(rest_depot>=fab_max[i]);

         }
        
        IloExpr rest_depott(env);
        for(int i=0;i<num_depot;i++){
            for(int j=0;j<num_depot;j++){
                      rest_depott+= var_dec[i][j];
            
            }
            problematransporte.add(rest_depott>=depot_max[i]);

        }
        
        
        
        if(cplex_transporte.solve()){
            
            cout << "solucoes para : " << cplex_transporte.getObjValue()<< endl;
        }
        
            
        cout<< "solucao ótima?" << cplex_transporte.getStatus()<< endl;
        
        cout<< cplex_transporte.getNrows()<< endl;
        cout<< cplex_transporte.getNcols()<< endl;
        
        
           for(int i=0;i<num_depot;i++){
                        for(int j=0;j<num_depot;j++)
                            cout<< cplex_transporte.getValue(var_dec[i][j])<< endl;
        
           }
        
    } catch (IloException iloe) {
        cerr << "Exception caught: " << iloe << endl;

    }catch (...) {
        cerr << "Unknown exception caught!" << endl;
    }
    
    
    env.end();
    
    return 0;
}
