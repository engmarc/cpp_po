//
//  problema7_padroes.cpp
//  mestrado_cplex
//
//  Created by marcos♞ on 27/07/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <stdio.h>
#include <ilcplex/ilocplex.h>
using namespace std;


int x0[]={1,1,7,2},
x1[]={2,2,3,3},
x2[]={1,0,9,2},
x3[]={1,4,4,1},
n_formas=4;
int main(){
    
    IloEnv env;
    
    try{
    IloModel problema_padroes(env);
    IloCplex cplex_padroes(env);
   
   IloExpr tampas(env);
   IloExpr corpo(env);//x[1,2]
   tampas+=x0[2]+x1[2]+x2[2]+x3[3];
   corpo+=x0[1]+x1[1]+x2[1]+x3[1];
    
    IloIntVarArray decidi_ai(env,n_formas,0,IloInfinity);
        
        
    //restricoes
        IloExpr tempo(env);//x[3]
        tempo+=decidi_ai[3]*(x0[3]+x1[3]+x2[3]+x3[3]);
        problema_padroes.add(tempo<=400);
        
        IloExpr por_tamanho(env);//x[0]
        por_tamanho+=decidi_ai[0]*(x0[0]+x2[0]+x3[0]);
        problema_padroes.add(por_tamanho<=200);
        IloExpr noventa(env);
        noventa+=decidi_ai[0]*x1[0];
        problema_padroes.add(noventa<=90);
        
        IloExpr somaqui(env);
        for(int i=0;i<n_formas;i++){
            
            somaqui+=decidi_ai[i]*(25*corpo - 3*tampas);
        }
        problema_padroes.add(IloMaximize(env,somaqui));
        

    cout << cplex_padroes.getObjValue()<< endl;
    cout<<cplex_padroes.getStatus();
        
    
       }catch(IloException e){
         cerr << "caught exception!!!"<<e<<endl;
     }catch(...){
         cerr << "Exception exception!!!";

     }
     
     
     
     env.end();
     
     return 0;
     
     
    
    
    
    

    
}
