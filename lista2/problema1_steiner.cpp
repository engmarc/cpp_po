//
//  problema1_steiner.cpp
//  mestrado_cplex
//
//  Created by marcos♞ on 26/07/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <ilcplex/ilocplex.h>
#include <list>
using namespace std;
int n_arestas,n_vertices;
int V1,V2;
int T,T1,L,R;
list<int> * a;
list<int>::iterator it;

void adj(int v1, int v2){
     a[v1].push_back(v2);
     a[v2].push_back(v1);
}


int main(){
    
    cin >> n_arestas;
    
    a= new list<int>[n_arestas];
    for(int i=0;i<n_arestas;i++){
        cout<< "escreva v1"<<endl;
        cin >> V1 >> V2;
        adj(V1, V2);
    }
    
  /*  cout << "escolha um terminal "<< endl;
    cin >> T;
    cout<<"terimal: " << T<< "tem " << a[T].size() << "vizinhos";
    */
    cout <<"digite o numero de vertices"<< endl;
    cin >> n_vertices;
    cout << "digite os terminais"<< endl;
    cin >> T;
    cin >> T1;
    IloEnv env;
    
    //cin >> L;
    //cin >>R;
    
    
    try {
        //verificando passagem de rotas
        bool visitados[n_vertices];
        for(int i=0;i<n_vertices;i++){
            visitados[i]=false;
        }
        
        if(!visitados[T])
               {
                   visitados[T] = true; // marca como visitado
                   // insere "v" na pilha
               }
        
        //implementacao do model cplex
        IloModel problema_steiner(env,"problema steiner");
        IloCplex cplex_steiner(problema_steiner);
        //criando uma lista das arestas entre os vertices
        a= new list<int>[n_arestas];
        
        //variavel de decisao
        IloIntVarArray x(env,n_vertices);
        IloExpr y(env);
        for(it =a[T].begin(); it != a[T].end(); it++){
            
            if(!visitados[*it] && !visitados[T1]){
            y+=x[*it];
            }

        }
        problema_steiner.add(IloMinimize(env,y));
        
        
        //restricoes
        IloExpr link(env);
        for(it =a[T].begin(); it != a[T].end(); it++){
            
            
        }
        
        
        if(cplex_steiner.solve()){
            
            cout<< cplex_steiner.getObjValue();
        }
        
        
        
    } catch (IloException e) {
        cerr<< "caugh exception"<< e<<endl;
    }catch(...){
        
        cerr<<"excecao da excecao"<<endl;
    }
    
    
    env.end();
    return 0;
}
