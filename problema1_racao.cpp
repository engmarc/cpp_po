//
//  problema1_racao.cpp
//  projeto prv-agv
//
//  Created by marcos♞ on 30/06/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//
#include <stdio.h>
#include <ilcplex/ilocplex.h>

using namespace std;

int main()
{
    // Criando o ambiente
    IloEnv env;
    try
    {
        //entrada do problema
        //poderia pedir como entrada do usuario
        int num_ingredientes= 2;
        int custo_amgs_xre[2]={20,30};
        int uso_de_carne[2]={1,4};
        int uso_de_cereal[2]={5,2};
        int limite_carne=10000;
        int limite_cereais=20000;
        
        
        IloModel problema_da_racao(env,"problema da racao");
        IloCplex cplex_racao(problema_da_racao);
        
        IloNumVarArray expressao_de_decisao_x(env,num_ingredientes,0,IloInfinity);
        
        //Funcao objetivo
        IloExpr funcao_somatorio(env);
        for(int i=0; i<num_ingredientes;i++){
            funcao_somatorio+= custo_amgs_xre[i]*expressao_de_decisao_x[i];
            
            //cout<< funcao_somatorio<< endl;
        }
        problema_da_racao.add(IloMaximize(env,funcao_somatorio));
        //maximizacao da funcao objetivo termina aqui
        
        //restricoes
        IloExpr restricao_carne(env);
        for(int i=0;i<num_ingredientes;i++){
            restricao_carne+=uso_de_carne[i]*expressao_de_decisao_x[i];
            //cout<< restricao_carne<<endl;
        }
        problema_da_racao.add(restricao_carne<=limite_carne);
        //restricao da carne termina aqui
        
        IloExpr restricao_cereal(env);
        for(int i=0;i<num_ingredientes;i++){
            restricao_cereal= uso_de_cereal[i]*expressao_de_decisao_x[i];
            
            //cout<< restricao_cereal<< endl;
        }
        problema_da_racao.add(restricao_cereal<=limite_cereais);
        //restricao do cereal termina aqui
        
        //utilizar metodo solver para obter solucao da heuristica
        
        if(cplex_racao.solve()){
            cout<< "achamos a solucao otima!! "<< cplex_racao.getObjValue()<< endl;
        }
        //obtendo a solucao
        IloNumArray solucao(env, num_ingredientes);
        cplex_racao.getValues(solucao,expressao_de_decisao_x);
        
        //imprimindo a solucao
        //nao sei se coloco lucro nas vendas ou lucro por venda!??!
        for(int i=0; i<num_ingredientes;i++){
            
            cout<< "Lucro nas vendas: "<<" : "<< solucao[i]<<endl;
        }
        
        
        
        
    }catch (const IloException e)
          {
              cerr << "Exception caught: " << e << endl;
          }
          catch (...)
          {
              cerr << "Unknown exception caught!" << endl;
          }

    env.end();
    return 0;
}
