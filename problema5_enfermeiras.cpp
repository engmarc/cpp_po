//
//  problema5_enfermeiras.cpp
//  mestrado_cplex
//
//  Created by marcos♞ on 25/07/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <stdio.h>
#include <ilcplex/ilocplex.h>

using namespace std;


int main(){
    
    
    
    IloEnv env;
    
    try{
        int dias=7;
        int total_enf[]={4,6,2,3,4,5,7};
        IloModel problema_enfermeira(env,"Problema das enfermeiras");
        
        IloCplex cplex_enfermeira(problema_enfermeira);
        
        IloNumVarArray var_(env,dias,0,dias);

        //restricoes
        IloExpr restricao_dia(env);
        for(int i=0;i<dias;i++){
            restricao_dia+=var_[i]*total_enf[i-3];

        problema_enfermeira.add(restricao_dia<=total_enf[i]);
        }
        
        
        IloExpr escala_h(env);
        for(int i=0;i<dias;i++){
            escala_h+=var_[i]*total_enf[i];
        }
        problema_enfermeira.add(IloMinimize(env,escala_h));
        
        cout<< cplex_enfermeira.getObjValue()<< endl;;
        cout << cplex_enfermeira.getStatus()<< endl;
        
    }catch(IloException e){
        
         cerr << "Exception caught: " << e << endl;
             }
             catch (...)
             {
                 cerr << "Unknown exception caught!" << endl;
             }
    
    
    env.end();
    
    return 0;
    
}
