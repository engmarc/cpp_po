//
//  problema4_fluxo.cpp
//  mestrado_cplex
//
//  Created by marcos♞ on 15/07/20.
//  Copyright © 2020 Marcos Bento. All rights reserved.
//

#include <ilcplex/ilocplex.h>
#include <stdio.h>
using namespace std;
int main() {
    IloEnv env;
    int vertices=6;

    int capacidade[6][6]={
        {0,11,12,0,0,0},
        {0,0,0,12,0,0},
        {0,1,0,11,0,0},
        {0,0,0,0,5,19},
        {0,0,0,7,0,4},
        {0,0,0,0,0,0}
    };
    
    
    try {
        
        //criando o modelo
        IloModel problema_fluxo(env, "Problema do fluxo maximo");
        IloCplex cplex_fluxo(problema_fluxo);

        IloArray<IloNumVarArray> decisao(env,vertices);
        
        for(int i=0; i<vertices;i++){
            decisao[i] = IloNumVarArray(env,vertices,0,IloInfinity);
        }
        
        IloExpr fun_obj(env);
        IloExpr auxiliar(env);
        for(int i=0;i<vertices;i++){
            for(int j=0;j<vertices;j++){
                fun_obj+=decisao[i][j]*capacidade[i][j];
               auxiliar+=capacidade[i][j];
            }
        }
        problema_fluxo.add(IloMaximize(env,fun_obj));
        
        problema_fluxo.add(fun_obj<=auxiliar);
        
      
        
         if (cplex_fluxo.solve()) {
            env.out() << "Solucao... " << cplex_fluxo.getObjValue() << endl;
         
        }
        
        cout << cplex_fluxo.getStatus()<< endl;
        



    }
    catch (const IloException e) {
        cerr << "Exception caught: " << e << endl;
    }
    catch (...) {
        cerr << "Unknown Exception caught! " << endl;
    }
        env.end();
    
    return 0;
}
